const btn = document.querySelector("#btn")
const ipUrl = "https://api.ipify.org/?format=json"
const div = document.querySelector("#root")

async function findIp() {
    const ipFetch = await fetch(ipUrl)
    const dataIp = await ipFetch.json()
    const findGeo = await fetch(`http://ip-api.com/json/${dataIp.ip}?lang=ru&fields=continent,country,region,city,district`)
    const geo = await findGeo.json()
    div.insertAdjacentHTML("afterbegin",`
    <div>
    <p>Your IP adress is ${dataIp.ip}</p>
    <p>${geo.continent}</p>
    <p>${geo.country}</p>
    <p>${geo.region}</p>
    <p>${geo.city}</p>   
</div>    
    `)
}

btn.addEventListener("click", findIp)