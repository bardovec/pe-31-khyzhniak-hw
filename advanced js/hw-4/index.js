const URL = "https://ajax.test-danit.com/api/swapi/films"
let filmWrapper = document.querySelector('.film-wrapper')
let filmCharacters = document.querySelector('.film-characters')


function getFilmList() {
    fetch(URL)
        .then(res => res.json())
        .then(response => {
            const result = [...response]
            const sortedResult = result.sort((a, b) => a.episodeId - b.episodeId)
            const filmList = sortedResult.map((item) => {
                const wrapperItem = document.createElement('div')
                wrapperItem.style.border = "1px solid black"
                const filmId = document.createElement('p')
                const filmName = document.createElement('p')
                const filmDescription = document.createElement('p')
                filmId.innerText = `Episode ${item.episodeId}`
                filmName.innerText = `Name ${item.name}`
                filmDescription.innerText = `Description ${item.openingCrawl}`
                filmWrapper.append(wrapperItem)
                wrapperItem.append(filmId, filmName, filmDescription)
                return wrapperItem

            })
        })
    getCharacters()
}


function getCharacters(index = 0) {
    fetch(URL)
        .then(response => response.json())
        .then(response => {
            const result = [...response]
            const sortedResult = result.sort((a, b) => a.episodeId - b.episodeId)
            const charactersList = sortedResult.map(item => {
                const charactersArr = [...item.characters]
                return charactersArr
            })
            const test = charactersList.map(arr =>{
                let divChildren = filmWrapper.children[index]
                let ul = document.createElement('ul')
                ul.innerText = 'Characters list:'
                let arrList = arr.map(data =>{
                    let li = document.createElement('li')

                    fetch(data)
                        .then(data => data.json())
                        .then(data => {

                            li.innerText = `${data.name}`
                            ul.append(li)
                        })
                    ul.append(li)

                })
                divChildren.append(ul)
                filmCharacters.append(divChildren)

            })

        })
    btn.setAttribute('disabled', false)
}

let btn = document.querySelector('.btn')
btn.addEventListener("click", getFilmList)
