const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];
const div = document.getElementById('root')

const renderBook = (arrayBooks) => {
    arrayBooks.forEach((book) => {

        const [validationResult, emptyFields] = validateFields(book)
        try {
            if (!validationResult) {
                throw new Error(`No fields:${emptyFields.join(", ")}`)

            } else {
                createElement(book);

            }
        } catch (error) {
            console.log(error)
        }

    });
}
const validateFields = (book) => {
    const requireFields = [
        'author',
        'name',
        'price'
    ];
    const bookFields = Object.keys(book);
    let validationResult = true;
    const emptyFields = [];
    if (requireFields.length === bookFields.length){
        validationResult = true;
    }
    requireFields.forEach((requireField) => {
        const isFieldInBook = bookFields.includes(requireField);
        if (!isFieldInBook) {
            emptyFields.push(requireField);
            validationResult = false;
        }

    })
    return [validationResult, emptyFields];
}


const createElement = (book) => {
    for(let key in book) {
        const ul = document.createElement('ul');
        const li = document.createElement('li');
        li.innerHTML = book[key];
        ul.appendChild(li);
        div.appendChild(ul);

    }

}

renderBook(books);
