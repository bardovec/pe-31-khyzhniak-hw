class Employee {
    constructor(name, age, salary, ) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get userName () {
        return this._name
    }
    set userName(value) {
        return this._name = value;
    }

    get userAge () {
        return this._age
    }
    set userAge(value) {
        return this._age = value;
    }
    get userSalary () {
        return this._salary
    }
    set userSalary(value) {
        return this._salary = value;
    }
}
class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get salary() {
        return this._salary * 3;
    }

}
const andriy = new Programmer("Andrii Khyzhniak", 25, 50000, 'JS');
const alex = new Programmer("Olexandr Forkin", 19, 10000, 'JS');
const serg = new Programmer("Sergio", 55, 100000, 'Assembler');
