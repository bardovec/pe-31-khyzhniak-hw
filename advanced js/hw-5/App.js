const usersURl = "https://ajax.test-danit.com/api/json/users";
const postsURl = "https://ajax.test-danit.com/api/json/posts";
const div = document.querySelector("#root")
const btn = document.querySelector("#close_btn")


window.onload = function data() {
    fetch(usersURl)
        .then(res => res.json())
        .then(response => {
            const result = [...response]

            const userList = result.map((user) => {
                return {
                    name: user.name,
                    email: user.email,
                    id: user.id

                }


            })

            fetch(postsURl)
                .then(res => res.json())
                .then(response => {
                    const result = [...response]

                    const postList = result.map((post) => {
                        return {
                            title: post.title,
                            body: post.body,
                            userId: post.userId
                        }

                    })
                    new Card(userList, postList).render()
                    deleteCard(postList)
                })

        })

}


class Card {
    constructor(users, posts) {
        this.users = users;
        this.posts = posts


    }

    render() {
        this.users.forEach(user => {
            this.posts.forEach(post => {

                if (post.userId === user.id) {
                    // renderCards(user, post)
                    div.insertAdjacentHTML("afterbegin", `
            <div class="post_card container">
            <div class="post_card_name_title">
            <p class="user_name ">name: ${user.name}</p>
            <p class="user_email ">email: ${user.email} <p id="close_btn" class="close"></p> </p>
            </div>
            <h2>Title</h2>
            <h3 class="card_title">${post.title}</h3>
            <h2 class="card_description">Description</h2>
            <p>${post.body}</p>
</div>`)
                }

            })
        })
    }

}

function deleteCard(post) {

   const data = post.forEach(post => console.log(post.userId))


}


// btn.addEventListener("click", (post) =>{
//
//     const postId = posts.map(post => post.id)
//     console.log(postId)
//
//
//     fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
//         method: 'DELETE',
//     })
//
//         .then(data =>
//             console.log(data)
//         );
//
//
// })
