
$(function toTop() {
    $("body").append(`<a id = "toTop"></a>`);
    btn = $("#toTop");

    $(window).scroll(function () {
        if ($(window).scrollTop() > $ (window).height()){
            btn.addClass("show");
        }else {
            btn.removeClass("show")
        }
    });
    btn.on("click", function (e){
        e.preventDefault();
        $('html, body').animate({scrollTop:1}, 2000)
    });
})


$(function anchor() {
    $('#MP').on('click', function (e) {
        $('html,body').stop().animate({scrollTop: $('.most-popular-posts').offset().top}, 1000);
        e.preventDefault();
    });
    $('#OMPC').on('click', function (e) {
        $('html,body').stop().animate({scrollTop: $('.popular-clients').offset().top}, 2000);
        e.preventDefault();
    });
    $('#TR').on('click', function (e) {
        $('html,body').stop().animate({scrollTop: $('.top-rated').offset().top}, 2000);
        e.preventDefault();
    });
    $('#HN').on('click', function (e) {
        $('html,body').stop().animate({scrollTop: $('.hot-news').offset().top}, 2000);
        e.preventDefault();
    });
});


// $('#content_toggle').click(function(){
//     $('.most-popular-posts').slideToggle(2000);
// });

