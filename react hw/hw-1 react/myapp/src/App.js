import './App.css';
import React, {Component} from 'react';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";


class App extends Component {
    state = {
        isOpenFirstModal: false,
        isOpenSecondModal: false

    }

    openFirstModal = () => {
        this.setState({isOpenFirstModal: !this.state.isOpenFirstModal})

    }
    openSecondModal = () => {
        this.setState({isOpenSecondModal: !this.state.isOpenSecondModal})

    }

    render() {
        const {isOpenFirstModal, isOpenSecondModal} = this.state
        return (
            <>
                {isOpenFirstModal && <Modal className='modalOne modal'
                                            header={'Do you want to delete this file'}
                                            text={'Once, you delete this file, it wont be possible to undo this action'}
                                            footer={'Do you want to delete this file?'}
                                            action={this.openFirstModal}/>}
                {isOpenSecondModal &&
                <Modal className={'modalTwo modal'} text={'This is the second modal window'} action={this.openSecondModal}/>}
                <div className={'buttn'}>
                    <Button onClick={this.openFirstModal} className={'btnOne btn'} text={'open first modal'}/>
                    <Button onClick={this.openSecondModal} className={'btnTwo btn'} text={'open second modal'}/>
                </div>

            </>
        );

    }

}

export default App;
