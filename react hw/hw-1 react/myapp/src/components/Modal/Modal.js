import React, {Component} from 'react';
import Button from "../Button/Button";
import './Modal.scss'
import CloseButton from 'react-bootstrap/CloseButton'
import {isDisabled} from "bootstrap/js/src/util";

class Modal extends Component {
    state = {
        CloseButton: false
    }

    render() {
        const {action, text, footer, header, className} = this.props

        return (
            <div className={className} onClick={action}>
                <div className={'header__modal'}>
                    <CloseButton />
                    <h2 className={'header__modal__text text_color'}>{header}</h2>
                    <p className={'text_color'}>{text}</p>
                    <p className={'text_color'}>{footer}</p>
                    <div className={'btn_modal'}>
                        <Button className={'btnOne_Modal btn'} text={'ok'}/>
                        <Button className={'btnTwo_Modal btn'} text={'cancel'}/>
                    </div>
                </div>
            </div>


        );
    }
}

export default Modal;