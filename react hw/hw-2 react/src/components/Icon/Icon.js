import React from 'react';
import * as Icons from '../../themes/icons'
import './Icon.scss'

const Icon = ({type, color, filled, onClick}) => {
    const jsx = Icons[type];

    if (!jsx) {
        return null;
    }

    return (
        <div className={`icon icon--${type}`} onClick={onClick}>
            {jsx({color, filled})}
        </div>
    )
};
export default Icon;