import React, {Component} from 'react';
import "./Button.css"

class Button extends Component {
    render() {
        const {text, onClick, className} = this.props
        return (
                <button className={className}  onClick={onClick} >{text}</button>


        );
    }
}

export default Button;