import React, {Component} from 'react';
import "./Products.scss"
import Button from "../Button/Button";
import Icon from "../Icon/Icon";
import Modal from "../Modal/Modal";


class Products extends Component {
    state = {

        iconFilled: false
    }
    componentDidMount() {
        const {favourites, name } = this.props;
        if (localStorage.hasOwnProperty("favourites:")) {
            if (localStorage.getItem("favourites:").includes(name)) {
                if (!favourites.includes(name)) {
                    favourites.push(name)
                }
                this.setState({iconFilled: !this.state.iconFilled})
            }
        }
    }


    render() {
        const {product, handleModal1, showModal1, closeModal1, favourites, name} = this.props
        const addToFavourite = () => {

            if (!favourites.includes(name)) {
                favourites.push(name)
            }

            localStorage.setItem('favourites:', favourites);
            this.setState({iconFilled: !this.state.iconFilled});
            removeFav()

        }

        const removeFav = () => {

            if (favourites.includes(name) && this.state.iconFilled) {
                favourites.splice(favourites.indexOf(name), 1)
                console.log(favourites)
            }
            localStorage.setItem('favourites:', favourites);

        }



        return (
            <>
                <div className="card-item">

                    <div><img className='card_img' src={product.img} alt=""/>

                        <Icon onClick={addToFavourite} filled={this.state.iconFilled} type='star'/>
                        <h3 className='card-title'>{product.name} </h3></div>
                    <div className='card_description'>

                        <p>Price: {product.price}$</p>
                        <p>Art: {product.art}</p>
                        <p>Color: {product.color}</p>

                    </div>
                    <Button onClick={(e) => {
                        handleModal1(e);

                    }
                    }
                            text="Add to card"></Button>
                </div>
                {showModal1 &&
                <Modal
                    id={product.id}
                    header="Отлично!"
                    text="Товар добавлен в корзину"
                    className="modal-window"
                    closeModal={closeModal1}
                    showModal={showModal1}
                    cardTitle={product.title}
                />}

            </>

        )


    }


}


// const Products = ({product}) => {
// console.log(product.img)
//     return (
//
//
//             <div className="card-item" >
//
//            <div><img className='card_img' src={product.img} alt=""/><h3 className='card-title'>{product.name}</h3></div>
//
//             <div className='card_description'>
//
//                 <p>Price: {product.price}$</p>
//                 <p>Art: {product.art}</p>
//                 <p>Color: {product.color}</p>
//
//             </div>
//                 <Button>{}</Button>
//             </div>
//
//
//
//
// )
// }

export default Products;