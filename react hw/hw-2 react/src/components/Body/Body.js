import React, {Component} from 'react';
import Products from "../Products/Products";
import './Body.scss'

class Body extends Component {
    render() {
        const {products, handleModal1,
            showModal1,
            closeModal1 ,
            favourites
        } = this.props
        const productCards = products.map(product => <Products key={product.id}
                                                               favourites={favourites}
                                                               name={product.name}
                                                               product={product}
                                                               handleModal1 ={handleModal1}
                                                               showModal1 = {showModal1}
                                                               closeModal1 = {closeModal1}/>)
        return (
            <div className='product_cards_items'>
                {products.length === 0 && <h4>You don't have any products yet</h4>}
                {products.length === 0 ? null : productCards}
            </div>

        )


    }
}


// const Body = ({products}) => {
// const productCards = products
//     .map(product => <Products key={product.id} product={product}/> )
//
// return (
//     <div className='product_cards_items'>
//         {products.length === 0 && <h4>You don't have any products yet</h4>}
//         {products.length === 0 ? null :  productCards }
//     </div>
// )
//
//
// };
//
export default Body;