import './App.css';
import React, {Component} from 'react';
import axios from "axios";
import Body from "./components/Body/Body";
import Loader from "./components/Loader/Loader";



class App extends Component {
    state = {
        favourites: [],
        showModal1: false,
        products: [],
        isLoading: true
    }
    handleModal1 = e => {
        if (this.state.showModal1) {
            this.closeModal1();
        }
        this.setState({showModal1: true});
        e.stopPropagation();
        document.addEventListener("click", this.closeModal1);

    };

    closeModal1 = () => {
        this.setState({showModal1: false});
        document.removeEventListener("click", this.closeModal1);
    };


    componentDidMount() {
        axios('http://localhost:3000/api/products.json')

            .then(res => {
                this.setState({products: res.data})
                this.setState({isLoading: false})
            })
    }

    render() {

        return (
            <div className="App">
                {this.state.isLoading ?
                    <Loader/> :
                    <Body
                        products={this.state.products}
                        handleModal1 ={this.handleModal1}
                        showModal1 = {this.state.showModal1}
                        closeModal1 = {this.closeModal1}
                        favourites={this.state.favourites}



                    ></Body>}
            </div>
        )


    }
}

export default App;


// const App = () => {
//     const [products, setProducts]= useState([])
//     const [isLoading, setIsLoading]= useState(true)
//     useEffect(() => {
//         axios('http://localhost:3000/api/products.json')
//
//             .then(res => {
//                 setProducts(res.data);
//                 setIsLoading(false)
//
//             })
//     }, []);
//
//     return (
//             <div className="App">
//                 {isLoading ?
//                     <Loader/> :
//                     <Body products={products}></Body>}
//             </div>
//     )
// }


//     render() {
//         const {products, isOpenFirstModal, isOpenSecondModal} = this.state
//
//         return (
//             <>
//                 {isOpenFirstModal && <Modal className='modalOne modal'
//                                             header={'Do you want to delete this file'}
//                                             text={'Once, you delete this file, it wont be possible to undo this action'}
//                                             footer={'Do you want to delete this file?'}
//                                             action={this.openFirstModal}/>}
//                 {isOpenSecondModal &&
//                 <Modal className={'modalTwo modal'} text={'This is the second modal window'}
//                        action={this.openSecondModal}/>}
//                 <div className={'buttn'}>
//                     <Button onClick={this.openFirstModal} className={'btnOne btn'} text={'open first modal'}/>
//                     <Button onClick={this.openSecondModal} className={'btnTwo btn'} text={'open second modal'}/>
//                     <Products products={products}/>
//                 </div>
//
//
//             </>
//         );
//
//     }
//
// }
//
//
// export default App;
